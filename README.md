![截图](./assets/icon.png)
# electron-start

#### 介绍
Electron 快速开始，应用打包

#### 软件截图
![截图](./screenshot/1.png)

#### 运行 / 打包
- `npm start`
- `npm package`

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request