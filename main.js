const { app, BrowserWindow } = require('electron')
const path = require('path')

function createWindow() {
    let win = new BrowserWindow({
        width: 720,
        height: 480,
        // fullscreen: true,
        show: false,
        resizable: true,
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true,
            devTools: true,
            webSecurity: false,
            preload: path.join(__dirname, 'preload.js')
        }
    })

    win.setMenu(null)
    win.setResizable(false);
    // win.maximize()
    win.setTitle("Electron Start")
    win.loadFile(__dirname + '/index.html')
    // win.loadURL("https://www.baidu.com");
    win.show()
}

app.whenReady().then(createWindow)